#! /usr/bin/env bash

echo "----- Install Django ------"
sudo apt update
sudo apt install python3-pip -y
sudo pip3 install Django

echo "----- Install Supervisor ------"
sudo apt install supervisor

echo "----- Clone Project and install requirements ------"
git clone https://gitfront.io/r/deusops/gYR9SeouZXyd/django-locallibrary.git
pip3 install -r /home/vagrant/django-locallibrary/requirements.txt
sleep 5

echo "----- Settings Connect DB ------"
sed -i "/ALLOWED_HOSTS/s/\[\([^]]*\)\]/['$ip_nginx', \1]/" /home/vagrant/django-locallibrary/locallibrary/settings.py
sed -i "s/'NAME': 'mydatabase'/'NAME': '$DB_NAME'/g" /home/vagrant/django-locallibrary/locallibrary/settings.py
sed -i "s/'USER': 'mydatabaseuser'/'USER': '$DB_USER'/g" /home/vagrant/django-locallibrary/locallibrary/settings.py
sed -i "s/'PASSWORD': 'mypassword'/'PASSWORD': '$DB_PASSWORD'/g" /home/vagrant/django-locallibrary/locallibrary/settings.py
sed -i "s/'HOST': '127.0.0.1'/'HOST': '$ip_db'/g" /home/vagrant/django-locallibrary/locallibrary/settings.py
sed -i "s/'PORT': '5432'/'PORT': '$db_port'/g" /home/vagrant/django-locallibrary/locallibrary/settings.py

echo "----- Settings Supervisor ------"
sudo cat > /etc/supervisor/conf.d/django.conf <<EOF
[program:django]
command=/usr/bin/python3 /home/vagrant/django-locallibrary/manage.py runserver 0.0.0.0:8000
directory=/home/vagrant/django-locallibrary/
autostart=true
autorestart=true
stderr_logfile=/var/log/supervisor/django.err.log
stdout_logfile=/var/log/supervisor/django.out.log
user=vagrant
EOF

echo "----- Python3 Migrate 2 ------"
cd /home/vagrant/django-locallibrary/ && python3 manage.py migrate

sudo systemctl enable supervisor --now
sleep 5
sudo systemctl restart supervisor
sudo systemctl status supervisor
