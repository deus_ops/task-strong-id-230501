#! /usr/bin/env bash

echo "----- Install PostgreSQL ------"
sudo apt update
sudo apt install postgresql postgresql-contrib -y

echo "----- Access Setting ------" 
sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" /etc/postgresql/12/main/postgresql.conf
echo 'host    aisks   aisksuser   192.168.56.1/24   md5' >> /etc/postgresql/12/main/pg_hba.conf

sudo service enabled postgresql
sudo service postgresql restart

echo "-------- Create DB for Keycloak -------"
sudo -u postgres psql -c "create database $DB_NAME;"
sudo -u postgres psql -c "create user $DB_USER with password '$DB_PASSWORD';"
sudo -u postgres psql -c "grant all privileges on database $DB_NAME to $DB_USER;"

sudo service postgresql status