#! /usr/bin/env bash

echo "----- Install Nginx ------"
sudo apt update
sudo apt install nginx -y

echo "----- Create Config Nginx ------"
sudo cat > /etc/nginx/sites-available/default <<EOF
server {
    listen 80;
    listen [::]:80;

    server_name _;

    location / {
        proxy_pass http://192.168.56.150:8000;
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    }
}
EOF
sudo ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/

echo "----- Restart and Status Nginx ------"
sudo systemctl restart nginx
sudo systemctl status nginx

